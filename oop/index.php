<?php
    require_once 'Animal.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    $sheep = new Animal("shaun");
    echo $sheep->getName(); // "shaun"
    echo $sheep->getLegs(); // 4
    echo $sheep->getCold_Blooded(); // no

    echo "<br>";
    $sungokong = new Ape("kera sakti");
    echo $sungokong->yell(); // "Auooo"

    echo "<br>";
    $kodok = new Frog("buduk");
    echo $kodok->jump(); // "hop hop"
    
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>